package com.example.demo.service;

import com.example.demo.domain.OutsourcedPart;
import com.example.demo.domain.Part;
import com.example.demo.repositories.OutsourcedPartRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OutsourcedPartServiceTest {
    OutsourcedPartRepository outsourcedPartRepository;
    OutsourcedPartService outsourcedPartService;
    @BeforeEach
    void setUp() {
        outsourcedPartRepository=mock(OutsourcedPartRepository.class);
        outsourcedPartService=new OutsourcedPartServiceImpl(outsourcedPartRepository);
    }

    @Test
    void findAll() {
        OutsourcedPart part=new OutsourcedPart();
        List partData=new ArrayList();
        partData.add(part);
        when(outsourcedPartRepository.findAll()).thenReturn(partData);
        List<OutsourcedPart> parts=outsourcedPartService.findAll();
        assertEquals(partData.size(),1);
    }
}