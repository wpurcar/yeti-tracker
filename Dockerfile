# Stage 1: Build the application
FROM ubuntu:22.04 AS build

RUN apt-get update && \
    apt-get install -y curl maven openjdk-17-jdk

WORKDIR /app

COPY . .

RUN mvn clean package

# Stage 2: Run the application
FROM ubuntu:22.04

RUN apt-get update && \
    apt-get install -y openjdk-17-jre nginx

WORKDIR /app

COPY --from=build /app/target/demo-0.0.1-SNAPSHOT.jar .

# Copy the nginx.conf file
COPY nginx.conf /etc/nginx/conf.d/yetitracker.conf

CMD service nginx start && java -jar demo-0.0.1-SNAPSHOT.jar