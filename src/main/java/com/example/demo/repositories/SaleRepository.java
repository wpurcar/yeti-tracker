package com.example.demo.repositories;

import com.example.demo.domain.Sale;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SaleRepository extends CrudRepository<Sale,Long> {
    List<Sale> findByProductId(Long productId);

    @Query("SELECT s FROM Sale s WHERE s.saleDate BETWEEN :startDate AND :endDate")
    List<Sale> findSalesBetweenDates(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

}
