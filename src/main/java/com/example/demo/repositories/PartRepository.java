package com.example.demo.repositories;

import com.example.demo.domain.Part;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PartRepository extends CrudRepository <Part,Long> {
    @Query("SELECT p FROM Part p WHERE LOWER(p.name) LIKE LOWER(concat('%', :keyword, '%'))")
    List<Part> search(@Param("keyword") String keyword);

}
