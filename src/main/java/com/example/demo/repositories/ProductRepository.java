package com.example.demo.repositories;

import com.example.demo.domain.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product,Long> {
    @Query("SELECT p FROM Product p WHERE LOWER(p.name) LIKE LOWER(concat('%', :keyword, '%'))")
    List<Product> search(@Param("keyword") String keyword);

    @Query("SELECT p.manufacturer, COUNT(p) FROM Product p GROUP BY p.manufacturer")
    List<Object[]> countProductsByManufacturer();
}
