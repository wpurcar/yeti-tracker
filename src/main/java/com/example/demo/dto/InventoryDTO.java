package com.example.demo.dto;

import com.example.demo.domain.Product;

import java.util.List;
import java.util.Map;

public class InventoryDTO {

    private String username;
    private String email;
    private List<Product> allProducts;
    private Long totalProductsCount;
    private List<Product> outOfStockProducts;
    private List<Product> lowInStockProducts;
    private Map<String, Long> manufacturerMap;

    public InventoryDTO(String username, String email, List<Product> allProducts, Long totalProductsCount, List<Product> outOfStockProducts, List<Product> lowInStockProducts, Map<String, Long> manufacturerMap) {
        this.username = username;
        this.email = email;
        this.allProducts = allProducts;
        this.totalProductsCount = totalProductsCount;
        this.outOfStockProducts = outOfStockProducts;
        this.lowInStockProducts = lowInStockProducts;
        this.manufacturerMap = manufacturerMap;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Product> getAllProducts() {
        return allProducts;
    }

    public void setAllProducts(List<Product> allProducts) {
        this.allProducts = allProducts;
    }

    public Long getTotalProductsCount() {
        return totalProductsCount;
    }

    public void setTotalProductsCount(Long totalProductsCount) {
        this.totalProductsCount = totalProductsCount;
    }

    public List<Product> getOutOfStockProducts() {
        return outOfStockProducts;
    }

    public void setOutOfStockProducts(List<Product> outOfStockProducts) {
        this.outOfStockProducts = outOfStockProducts;
    }

    public List<Product> getLowInStockProducts() {
        return lowInStockProducts;
    }

    public void setLowInStockProducts(List<Product> lowInStockProducts) {
        this.lowInStockProducts = lowInStockProducts;
    }

    public Map<String, Long> getManufacturerMap() {
        return manufacturerMap;
    }

    public void setManufacturerMap(Map<String, Long> manufacturerMap) {
        this.manufacturerMap = manufacturerMap;
    }
}
