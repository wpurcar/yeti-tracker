package com.example.demo.dto;

import com.example.demo.domain.Product;

import java.util.List;

public class SalesDTO {

    private String username;

    private String email;

    private double monthlySalesRevenue;

    private double yearToDateSales;

    private List<Product> topSellingProducts;

    private double inventoryTurnoverRate;

    public SalesDTO(
            String username,
            String email,
            double monthlySalesRevenue,
            double yearToDateSales,
            List<Product> topSellingProducts,
            double inventoryTurnoverRate) {
        this.username = username;
        this.email = email;
        this.monthlySalesRevenue = monthlySalesRevenue;
        this.yearToDateSales = yearToDateSales;
        this.topSellingProducts = topSellingProducts;
        this.inventoryTurnoverRate = inventoryTurnoverRate;
    }

    // Getters and Setters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getMonthlySalesRevenue() {
        return monthlySalesRevenue;
    }

    public void setMonthlySalesRevenue(double monthlySalesRevenue) {
        this.monthlySalesRevenue = monthlySalesRevenue;
    }

    public double getYearToDateSales() {
        return yearToDateSales;
    }

    public void setYearToDateSales(double yearToDateSales) {
        this.yearToDateSales = yearToDateSales;
    }

    public List<Product> getTopSellingProducts() {
        return topSellingProducts;
    }

    public void setTopSellingProducts(List<Product> topSellingProducts) {
        this.topSellingProducts = topSellingProducts;
    }

    public double getInventoryTurnoverRate() {
        return inventoryTurnoverRate;
    }

    public void setInventoryTurnoverRate(double inventoryTurnoverRate) {
        this.inventoryTurnoverRate = inventoryTurnoverRate;
    }
}
