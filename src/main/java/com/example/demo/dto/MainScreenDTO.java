package com.example.demo.dto;

import com.example.demo.domain.Part;
import com.example.demo.domain.Product;

import java.util.List;

public class MainScreenDTO {
    private String username;
    private String email;
    private List<PartDTO> partDTOList;
    private List<Product> products;
    private String partkeyword;
    private String productkeyword;

    // Constructor
    public MainScreenDTO(String username, String email, List<PartDTO> partDTOList, List<Product> products, String partkeyword, String productkeyword) {
        this.username = username;
        this.email = email;
        this.partDTOList = partDTOList;
        this.products = products;
        this.partkeyword = partkeyword;
        this.productkeyword = productkeyword;
    }

    // Getters and Setters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<PartDTO> getPartDTOList() {
        return partDTOList;
    }

    public void setPartDTOList(List<Part> parts) {
        this.partDTOList = partDTOList;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getPartkeyword() {
        return partkeyword;
    }

    public void setPartkeyword(String partkeyword) {
        this.partkeyword = partkeyword;
    }

    public String getProductkeyword() {
        return productkeyword;
    }

    public void setProductkeyword(String productkeyword) {
        this.productkeyword = productkeyword;
    }
}
