package com.example.demo.dto;

import com.example.demo.domain.Product;

import java.util.List;

public class ReportsDTO {

    private String username;

    private String email;

    public ReportsDTO(
            String username,
            String email) {
        this.username = username;
        this.email = email;
    }

    // Getters and Setters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
