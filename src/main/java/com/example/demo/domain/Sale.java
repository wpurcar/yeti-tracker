package com.example.demo.domain;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Sales")
public class Sale implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull(message = "Product ID is required")
    @Column(name = "product_id", nullable = false)
    private long productId;

    @Min(value = 1, message = "Quantity sold must be at least 1")
    @Column(name = "quantity_sold", nullable = false)
    private int quantitySold;

    @NotNull(message = "Date is required")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sale_date", nullable = false)
    private Date saleDate;

    @Positive(message = "Sale amount must be positive")
    @Column(name = "sale_amount", nullable = false)
    private double saleAmount;

    public Sale() {
    }

    public Sale(long productId, int quantitySold, Date saleDate, double saleAmount) {
        this.productId = productId;
        this.quantitySold = quantitySold;
        this.saleDate = saleDate;
        this.saleAmount = saleAmount;
    }

    public Sale(long id, long productId, int quantitySold, Date saleDate, double saleAmount) {
        this.id = id;
        this.productId = productId;
        this.quantitySold = quantitySold;
        this.saleDate = saleDate;
        this.saleAmount = saleAmount;
    }

    // Getters and Setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public int getQuantitySold() {
        return quantitySold;
    }

    public void setQuantitySold(int quantitySold) {
        this.quantitySold = quantitySold;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public double getSaleAmount() {
        return saleAmount;
    }

    public void setSaleAmount(double saleAmount) {
        this.saleAmount = saleAmount;
    }

    // Override equals, hashCode, and toString methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sale sale)) return false;

        return getId() == sale.getId();
    }

    @Override
    public int hashCode() {
        return (int) (getId() ^ (getId() >>> 32));
    }

    @Override
    public String toString() {
        return "Sale{" +
                "id=" + id +
                ", productId=" + productId +
                ", quantitySold=" + quantitySold +
                ", saleDate=" + saleDate +
                ", saleAmount=" + saleAmount +
                '}';
    }
}
