package com.example.demo.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("1")
public class InhousePart extends Part {
    Integer partId;

    public InhousePart() {
    }

    public InhousePart(String name, double price, int inv, String sku, int minInv, int maxInv) {
        super(name, price, inv);
        setSku(sku);
        setMinInv(minInv);
        setMaxInv(maxInv);
    }

    public Integer getPartId() {
        return partId;
    }

    public void setPartId(Integer partId) {
        this.partId = partId;
    }
}
