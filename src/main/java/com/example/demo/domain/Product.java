package com.example.demo.domain;

import com.example.demo.validators.ValidEnufParts;
import com.example.demo.validators.ValidProductPrice;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Products")
@ValidProductPrice
@ValidEnufParts
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotBlank(message =  "SKU is required")
    private String sku;

    @NotBlank(message = "Name is required")
    private String name;

    @NotBlank(message = "Manufacturer is required")
    private String manufacturer;

    @NotBlank(message = "Refrigerant is required")
    private String refrigerant;

    @Positive(message = "Price value must be positive")
    private double price;

    @Positive(message = "Inventory value must be positive")
    private int inv;

    @Min(value = 10, message = "Minimum inventory must be at least 10 units")
    private int minInv;

    @Max(value = 50, message = "Maximum inventory must not exceed 50 units")
    private int maxInv;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "products")
    private Set<Part> parts = new HashSet<>();

    public Product() {
    }

    public Product(String sku, String name, String manufacturer, String refrigerant, double price, int inv, int minInv, int maxInv) {
        this.sku = sku;
        this.name = name;
        this.manufacturer = manufacturer;
        this.refrigerant = refrigerant;
        this.price = price;
        this.inv = inv;
        this.minInv = minInv;
        this.maxInv = maxInv;
    }

    public Product(long id, String sku, String name, String manufacturer, String refrigerant,  double price, int inv, int minInv, int maxInv) {
        this.id = id;
        this.sku = sku;
        this.name = name;
        this.manufacturer = manufacturer;
        this.refrigerant = refrigerant;
        this.price = price;
        this.inv = inv;
        this.minInv = minInv;
        this.maxInv = maxInv;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getRefrigerant() {
        return refrigerant;
    }

    public void setRefrigerant(String refrigerant) {
        this.refrigerant = refrigerant;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getInv() {
        return inv;
    }

    public void setInv(int inv) {
        this.inv = inv;
    }

    public int getMinInv() {
        return minInv;
    }

    public void setMinInv(int inv) {
        this.minInv = inv;
    }

    public int getMaxInv() {
        return maxInv;
    }

    public void setMaxInv(int inv) {
        this.maxInv = inv;
    }

    public Set<Part> getParts() {
        return parts;
    }

    public void setParts(Set<Part> parts) {
        this.parts = parts;
    }

    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return id == product.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

}
