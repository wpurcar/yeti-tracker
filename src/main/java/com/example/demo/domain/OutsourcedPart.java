package com.example.demo.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("2")
public class OutsourcedPart extends Part {
    String manufacturer;

    public OutsourcedPart() {
    }

    public OutsourcedPart(String name, double price, int inv, String sku, String manufacturer, int minInv, int maxInv) {
        super(name, price, inv);
        setSku(sku);
        setManufacturer(manufacturer);
        setMinInv(minInv);
        setMaxInv(maxInv);

    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
}
