package com.example.demo.service;

import com.example.demo.domain.Part;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;

@Service
public class CsvReportService {

    @Autowired
    private PartService partService;

    public ByteArrayOutputStream generateLowStockReport() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(baos);

        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                .withHeader("Product ID", "Name", "Stock"));

        List<Part> lowStockParts = partService.getLowStockParts();
        for (Part part : lowStockParts) {
            csvPrinter.printRecord(part.getId(), part.getName(), part.getInv());
        }

        csvPrinter.flush();
        csvPrinter.close();

        return baos;
    }
}
