package com.example.demo.service;

import com.example.demo.domain.Product;
import com.example.demo.domain.Sale;
import com.example.demo.repositories.ProductRepository;
import com.example.demo.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
public class SaleServiceImpl implements SaleService {
    private final SaleRepository saleRepository;

    private final ProductRepository productRepository;

    @Autowired
    public SaleServiceImpl(SaleRepository saleRepository, ProductRepository productRepository) {
        this.saleRepository = saleRepository;
        this.productRepository = productRepository;
    }

    @Override
    public List<Sale> findAll() {
        return (List<Sale>) saleRepository.findAll();
    }

    @Override
    public Sale findById(Long theId) {
        Optional<Sale> result = saleRepository.findById(theId);

        if (result.isPresent()) {
            return result.get();
        } else {
            // we didn't find the sale id
            throw new RuntimeException("Did not find sale id - " + theId);
        }
    }

    @Override
    public void save(Sale theSale) {
        saleRepository.save(theSale);
    }

    @Override
    public void deleteById(Long theId) {
        saleRepository.deleteById(theId);
    }

    @Override
    public List<Sale> searchByKeyword(String keyword) {
        // Implement the search logic here
        // Example: return saleRepository.findBySomeFieldContaining(keyword);
        return null; // Replace with the actual search logic
    }

    @Override
    public double calculateMonthlySalesRevenue() {
        List<Sale> sales = (List<Sale>) saleRepository.findAll();
        double totalRevenue = 0.0;
        LocalDate startOfMonth = LocalDate.now().withDayOfMonth(1);
        Date startOfMonthDate = Date.from(startOfMonth.atStartOfDay(ZoneId.systemDefault()).toInstant());
        for (Sale sale : sales) {
            if (sale.getSaleDate().after(startOfMonthDate)) {
                totalRevenue += sale.getSaleAmount();
            }
        }
        return totalRevenue;
    }

    @Override
    public double calculateYearToDateSales() {
        List<Sale> sales = (List<Sale>) saleRepository.findAll();
        double totalRevenue = 0.0;
        LocalDate startOfYear = LocalDate.now().withDayOfYear(1);
        Date startOfYearDate = Date.from(startOfYear.atStartOfDay(ZoneId.systemDefault()).toInstant());
        for (Sale sale : sales) {
            if (sale.getSaleDate().after(startOfYearDate)) {
                totalRevenue += sale.getSaleAmount();
            }
        }
        return totalRevenue;
    }

    @Override
    public List<Product> findTopSellingProducts() {
        List<Sale> sales = (List<Sale>) saleRepository.findAll();

        // Map to hold total quantity sold for each product
        Map<Long, Integer> productSaleQuantities = new HashMap<>();
        int totalQuantitySold = 0;

        // Accumulate total quantity sold for each product
        for (Sale sale : sales) {
            productSaleQuantities.merge(sale.getProductId(), sale.getQuantitySold(), Integer::sum);
            totalQuantitySold += sale.getQuantitySold();
        }

        // Calculate average quantity sold
        double avgSold = (double) totalQuantitySold / productSaleQuantities.size();

        // Find products that sold more than the average
        List<Long> topSellingProductIDs = new ArrayList<>();
        for (Map.Entry<Long, Integer> entry : productSaleQuantities.entrySet()) {
            if (entry.getValue() > avgSold) {
                topSellingProductIDs.add(entry.getKey());
            }
        }

        // Fetch and return the top selling products
        List<Product> topSellingProducts = new ArrayList<>();
        for (Long productId : topSellingProductIDs) {
            // Assuming a method in productRepository to find product by ID
            productRepository.findById(productId).ifPresent(topSellingProducts::add);
        }

        return topSellingProducts;
    }


    @Override
    public double calculateInventoryTurnoverRate() {
        // Implement logic to calculate inventory turnover rate
        return 0.0;
    }
}
