package com.example.demo.service;

import com.example.demo.domain.Part;
import com.example.demo.domain.Product;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class PdfReportService {

    @Autowired
    private ProductService productService;
    @Autowired
    private PartService partService;

    public ByteArrayOutputStream generateLowStockReport() throws DocumentException {
        Document document = new Document();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfWriter.getInstance(document, baos);
        document.open();

        addTitlePage(document, "Parts Low In Stock");

        List<Part> lowStockParts = partService.getLowStockParts();
        addTable(document, lowStockParts, "Parts");

        document.close();
        return baos;
    }

    private void addTitlePage(Document document, String title) throws DocumentException {
        Paragraph preface = new Paragraph();
        addEmptyLine(preface, 1);
        preface.add(new Paragraph(title, FontFactory.getFont(FontFactory.HELVETICA_BOLD, 18)));
        preface.add(new Paragraph(
                "Report generated on: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
                FontFactory.getFont(FontFactory.HELVETICA, 12)));
        addEmptyLine(preface, 1);
        document.add(preface);
    }

    private <T> void addTable(Document document, List<T> items, String tableName) throws DocumentException {
        PdfPTable table = new PdfPTable(3); // three columns
        table.setWidthPercentage(100);
        table.setSpacingBefore(10f);
        table.setSpacingAfter(10f);

        // Set Column widths
        float[] columnWidths = {1f, 2f, 1f};
        table.setWidths(columnWidths);

        PdfPCell c1 = new PdfPCell(new Phrase(tableName + " ID"));
        c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Name"));
        c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Stock"));
        c1.setBackgroundColor(BaseColor.LIGHT_GRAY);
        table.addCell(c1);
        table.setHeaderRows(1);

        for (T item : items) {
            if (item instanceof Product product) {
                table.addCell(String.valueOf(product.getId()));
                table.addCell(product.getName());
                table.addCell(String.valueOf(product.getInv()));
            } else if (item instanceof Part part) {
                table.addCell(String.valueOf(part.getId()));
                table.addCell(part.getName());
                table.addCell(String.valueOf(part.getInv()));
            }
        }

        document.add(table);
    }

    private void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
