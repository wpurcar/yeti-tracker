package com.example.demo.service;

import com.example.demo.domain.Product;
import com.example.demo.domain.Sale;
import java.util.List;

public interface SaleService {
    List<Sale> findAll();
    Sale findById(Long theId);
    void save(Sale theSale);
    void deleteById(Long theId);
    List<Sale> searchByKeyword(String keyword);

    double calculateMonthlySalesRevenue();
    double calculateYearToDateSales();
    List<Product> findTopSellingProducts();
    double calculateInventoryTurnoverRate();

}
