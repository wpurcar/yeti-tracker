package com.example.demo.service;

import com.example.demo.domain.InhousePart;
import com.example.demo.domain.OutsourcedPart;
import com.example.demo.domain.Part;
import com.example.demo.dto.PartDTO;
import com.example.demo.repositories.PartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PartServiceImpl implements PartService{
        private final PartRepository partRepository;

        private final double multiplier = 1.33;

        @Autowired

    public PartServiceImpl(PartRepository partRepository) {
        this.partRepository = partRepository;
    }

    @Override
    public List<Part> findAll() {
        return (List<Part>) partRepository.findAll();
    }
    public List<Part> listAll(String keyword){
        if(keyword !=null){
            return partRepository.search(keyword);
        }
        return (List<Part>) partRepository.findAll();
    }
    @Override
    public Part findById(int theId) {
        Long theIdl=(long)theId;
        Optional<Part> result = partRepository.findById(theIdl);

        Part thePart = null;

        if (result.isPresent()) {
            thePart = result.get();
        }
        else {
            throw new RuntimeException("Did not find part id - " + theId);
        }

        return thePart;
    }

    @Override
    public void save(Part thePart) {
            partRepository.save(thePart);

    }

    @Override
    public void deleteById(int theId) {
        Long theIdl=(long)theId;
        partRepository.deleteById(theIdl);
    }

    public List<Part> getPartsSortedByInventoryRatio() {
        return StreamSupport.stream(partRepository.findAll().spliterator(), false)
                .sorted(Comparator.comparingDouble(part -> (double) part.getInv() / part.getMinInv()))
                .collect(Collectors.toList());
    }

    public List<Part> getLowStockParts() {
        return StreamSupport.stream(partRepository.findAll().spliterator(), false)
                .filter(part -> part.getInv() <= part.getMinInv() * multiplier)
                .collect(Collectors.toList());
    }
}

