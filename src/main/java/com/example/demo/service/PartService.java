package com.example.demo.service;

import com.example.demo.domain.InhousePart;
import com.example.demo.domain.OutsourcedPart;
import com.example.demo.domain.Part;
import com.example.demo.dto.PartDTO;

import java.util.List;

public interface PartService {
    List<Part> findAll();
    Part findById(int theId);
    void save (Part thePart);
    void deleteById(int theId);

    List<Part> listAll(String keyword);

    List<Part> getPartsSortedByInventoryRatio();

    List<Part> getLowStockParts();

    default PartDTO convertToPartDTO(Part part) {
        PartDTO dto = new PartDTO();
        dto.setId(part.getId());
        dto.setName(part.getName());
        dto.setPrice(part.getPrice());
        dto.setInv(part.getInv());
        dto.setMinInv(part.getMinInv());
        dto.setMaxInv(part.getMaxInv());

        if (part instanceof InhousePart) {
            dto.setPartType(1);
        } else if (part instanceof OutsourcedPart) {
            dto.setPartType(2);
        }

        return dto;
    }}


