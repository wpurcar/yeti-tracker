package com.example.demo.controllers;

import com.example.demo.domain.OutsourcedPart;
import com.example.demo.dto.ApiResponseDTO;
import com.example.demo.service.OutsourcedPartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin(origins = "https://localhost:4200")
@RestController
@RequestMapping("/api/outsourced")
public class OutsourcedPartController {

    @Autowired
    private ApplicationContext context;

    private final OutsourcedPartService outsourcedPartService;
    private static final Logger logger = LoggerFactory.getLogger(OutsourcedPartController.class);

    @Autowired
    public OutsourcedPartController(OutsourcedPartService outsourcedPartService) {
        this.outsourcedPartService = outsourcedPartService;
    }

    @GetMapping("/getpart")
    public ResponseEntity<?> getPart(@RequestParam("partId") int theId) {
        try {
            OutsourcedPart theOutsourcedPart = outsourcedPartService.findById(theId);
            return ResponseEntity.ok(theOutsourcedPart);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error fetching outsourced part with partId: " + theId);
        }
    }

    @PostMapping("/addpart")
    public ResponseEntity<?> addPart(@Valid @RequestBody OutsourcedPart outsourcedPart, Errors errors) {
        if (errors.hasErrors()) {
            String errorMsg = errors.getAllErrors().stream()
                    .map(ObjectError::getDefaultMessage)
                    .collect(Collectors.joining(", "));
            return ResponseEntity.badRequest().body(errorMsg);
        } else {
            String price = String.valueOf(outsourcedPart.getPrice()).replace(",", "");
            outsourcedPart.setPrice(Double.parseDouble(price));
            outsourcedPartService.save(outsourcedPart);

            Map<String, Object> response = new HashMap<>();
            response.put("message", "Outsourced part added successfully");
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        }
    }

    @PutMapping("/updatepart")
    public ResponseEntity<?> updatePart(@Valid @RequestBody OutsourcedPart outsourcedPart, Errors errors) {
        logger.debug("Calling updatePart with outsourcedPart: " + outsourcedPart);

        if (errors.hasErrors()) {
            String errorMsg = errors.getAllErrors().stream()
                    .map(ObjectError::getDefaultMessage)
                    .collect(Collectors.joining(", "));
            return ResponseEntity.badRequest().body(errorMsg);
        } else {
            try {
                long theId = outsourcedPart.getId();
                logger.debug("Fetched partId: " + theId);
                OutsourcedPart thePart = outsourcedPartService.findById(Math.toIntExact(theId));
                logger.debug("Fetched thePart: " + thePart);

                String price = String.valueOf(outsourcedPart.getPrice()).replace(",", "");
                outsourcedPart.setPrice(Double.parseDouble(price));
                outsourcedPartService.save(outsourcedPart);

                ApiResponseDTO<OutsourcedPart> apiResponseDTO = new ApiResponseDTO<>(true, "Outsourced part updated successfully.", outsourcedPart);
                return ResponseEntity.ok(apiResponseDTO);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error updating outsourced part with partId: " + outsourcedPart.getId());
            }
        }
    }
}