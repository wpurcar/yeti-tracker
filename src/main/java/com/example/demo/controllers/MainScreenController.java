package com.example.demo.controllers;

import com.example.demo.domain.*;
import com.example.demo.dto.MainScreenDTO;
import com.example.demo.dto.PartDTO;
import com.example.demo.repositories.*;
import com.example.demo.service.PartService;
import com.example.demo.service.ProductService;
import com.example.demo.service.UserDetailsImpl;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@CrossOrigin( origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class MainScreenController {
    private final PartService partService;
    private final ProductService productService;
    private final InhousePartRepository inhousePartRepository;
    private final OutsourcedPartRepository outsourcedPartRepository;
    private final PartRepository partRepository;
    private final ProductRepository productRepository;

    private final RoleRepository roleRepository;

    private final SaleRepository saleRepository;

//    @GetMapping("/about")
//    public String showAboutPage(Model model) {
//        return "about";
//    }

    public MainScreenController(PartService partService, ProductService productService,
                                InhousePartRepository inhousePartRepository, OutsourcedPartRepository outsourcedPartRepository,
                                PartRepository partRepository, ProductRepository productRepository, RoleRepository roleRepository,
                                SaleRepository saleRepository) {
        this.partService = partService;
        this.productService = productService;
        this.inhousePartRepository = inhousePartRepository;
        this.outsourcedPartRepository = outsourcedPartRepository;
        this.partRepository = partRepository;
        this.productRepository = productRepository;
        this.roleRepository = roleRepository;
        this.saleRepository = saleRepository;
    }

    @GetMapping("/mainscreen")
    public ResponseEntity<MainScreenDTO> listPartsandProducts(@Param("partkeyword") String partkeyword,
                                                              @Param("productkeyword") String productkeyword) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth == null || auth.getPrincipal().equals("anonymousUser")) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        addSampleInventory();
        addSampleSales();

        UserDetailsImpl userDetail = (UserDetailsImpl) auth.getPrincipal();
        String username = userDetail.getUsername();
        String email = userDetail.getEmail();

        List<Part> partList = partService.listAll(partkeyword);

        List<PartDTO> partDTOList = partList.stream()
                .map(partService::convertToPartDTO)
                .toList();

        List<Product> productList = productService.listAll(productkeyword);

        MainScreenDTO mainScreenDTO = new MainScreenDTO(username, email, partDTOList, productList, partkeyword, productkeyword);

        return ResponseEntity.ok(mainScreenDTO);
    }


    private void addSampleInventory() {
        if (partRepository.count() == 0 && productRepository.count() == 0) {

            InhousePart part1 = new InhousePart("20x20 Air Filter, MERV 8 pleated (5 pack)", 12.96, 94, "AF2020M85", 10, 200);
            InhousePart part2 = new InhousePart("40/5 μf 370 Volt VAC - Dual Run Capacitor", 43.25, 93, "DC370VAC405", 20, 200);
            InhousePart part3 = new InhousePart("5A Fuse, (5 pack)", 18.86, 95, "5AFUSE5", 20, 200);


            OutsourcedPart part4 = new OutsourcedPart("R-410A Refrigerant 25LB", 375.00, 25, "R410a25ig", "DuPont",  11, 50);
            OutsourcedPart part5 = new OutsourcedPart("Honeywell 2H/1C, Standard Display Thermostat", 53.18, 84, "TH3210D1004", "Honeywell", 30, 200);

            inhousePartRepository.save(part1);
            inhousePartRepository.save(part2);
            inhousePartRepository.save(part3);
            outsourcedPartRepository.save(part4);
            outsourcedPartRepository.save(part5);


            Product product1 = new Product("GSX140181", "18,000 BTU 1.5 Ton 14 SEER Goodman Air Conditioner Standard Split System", "Goodman", "R-410A", 1946.00, 12, 10, 50);
            Product product2 = new Product("GSX140241", "24,000 BTU 2 Ton 14 SEER Goodman Air Conditioner Split System ", "Goodman", "R-410A", 2568.00, 11, 10, 50);
            Product product3 = new Product("GSZ140301", "30,000 BTU 2.5 Ton 14 SEER Goodman Heat Pump Air Conditioner System", "Goodman", "R-410A", 2913.00, 8, 10, 50);
            Product product4 = new Product("GSZ140361", "36,000 BTU 3 Ton 14 SEER Goodman Heat Pump", "Goodman", "R-410A", 2196.00, 12, 10, 50);
            Product product5 = new Product("GSZ140421", "42,000 BTU 3.5 Ton 14 SEER Goodman Air Conditioner Split System", "Goodman", "R-410A", 2973.00, 9, 10, 50);

            Set<Part> product1Parts = new HashSet<>();
            product1Parts.add(part1);
            product1Parts.add(part2);
            product1.setParts(product1Parts);

            Set<Part> product2Parts = new HashSet<>();
            product2Parts.add(part3);
            product2Parts.add(part4);
            product2.setParts(product2Parts);

            Set<Part> product3Parts = new HashSet<>();
            product3Parts.add(part1);
            product3Parts.add(part3);
            product3.setParts(product3Parts);

            Set<Part> product4Parts = new HashSet<>();
            product4Parts.add(part2);
            product4Parts.add(part5);
            product4.setParts(product4Parts);

            Set<Part> product5Parts = new HashSet<>();
            product5Parts.add(part1);
            product5Parts.add(part4);
            product5.setParts(product5Parts);

            productRepository.save(product1);
            productRepository.save(product2);
            productRepository.save(product3);
            productRepository.save(product4);
            productRepository.save(product5);
        }
    }

    private void addSampleSales() {
        if (saleRepository.count() == 0 && productRepository.count() > 0) {
            List<Product> products = (List<Product>) productRepository.findAll();

            for (Product product : products) {
                // Sample data for sales
                int quantitySold = (int) (Math.random() * 10 + 1); // Random quantity between 1 and 10
                Date saleDate = new Date(); // Current date
                double saleAmount = product.getPrice() * quantitySold;

                Sale sale = new Sale(product.getId(), quantitySold, saleDate, saleAmount);
                saleRepository.save(sale);
            }
        }
    }

}

