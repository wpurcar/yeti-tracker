package com.example.demo.controllers;

import com.example.demo.domain.InhousePart;
import com.example.demo.dto.ApiResponseDTO;
import com.example.demo.service.InhousePartService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/inhouse")
public class InhousePartController {

    @Autowired
    private ApplicationContext context;

    private final InhousePartService inhousePartService;

    private static final Logger logger = LoggerFactory.getLogger(InhousePartController.class);

    @Autowired
    public InhousePartController(InhousePartService inhousePartService) {
        this.inhousePartService = inhousePartService;
    }

    @GetMapping("/getpart")
    public ResponseEntity<?> getPart(@RequestParam("partId") int theId) {
        try {
            InhousePart theInhousePart = inhousePartService.findById(theId);
            return ResponseEntity.ok(theInhousePart);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error fetch inhouse part with partId: " + theId);
        }
    }

    @PostMapping("/addpart")
    public ResponseEntity<?> addPart(@Valid @RequestBody InhousePart inhousePart, Errors errors) {
        if (errors.hasErrors()) {
            String errorMsg = errors.getAllErrors().stream()
                    .map(ObjectError::getDefaultMessage)
                    .collect(Collectors.joining(","));
            return ResponseEntity.badRequest().body(errorMsg);
        } else {
            String price = String.valueOf(inhousePart.getPrice()).replace(",", "");
            inhousePart.setPrice(Double.parseDouble(price));
            inhousePartService.save(inhousePart);

            Map<String, Object> response = new HashMap<>();
            response.put("message", "Inhouse part added successfully");
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        }
    }

    @PutMapping("/updatepart")
    public ResponseEntity<?> updatePart(@Valid @RequestBody InhousePart inhousePart, Errors errors) {
        logger.debug("Calling updatePart with inhouse part: " + inhousePart);

        if (errors.hasErrors()) {
            String errorMsg = errors.getAllErrors().stream()
                    .map(ObjectError::getDefaultMessage)
                    .collect(Collectors.joining(", "));
            return ResponseEntity.badRequest().body(errorMsg);
        } else {
            try {
                long theId = inhousePart.getId();
                logger.debug("Fetched inhouse part with partId: " + theId);
                InhousePart thePart = inhousePartService.findById(Math.toIntExact(theId));
                logger.debug("Fetched thePart: " + thePart);

                String price = String.valueOf(inhousePart.getPrice()).replace(",", "");
                inhousePart.setPrice(Double.parseDouble(price));
                inhousePartService.save(inhousePart);

                ApiResponseDTO<InhousePart> apiResponseDTO = new ApiResponseDTO<>(true, "Inhouse part updated successfully", thePart);
                return ResponseEntity.ok(apiResponseDTO);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error updating inhouse part with partId: " + inhousePart.getId());
            }
        }
    }

}