package com.example.demo.controllers;

import com.example.demo.domain.Product;
import com.example.demo.dto.InventoryDTO;
import com.example.demo.repositories.ProductRepository;
import com.example.demo.service.UserDetailsImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@CrossOrigin( origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class InventoryController {
    private final ProductRepository productRepository;

    public InventoryController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    @GetMapping("/inventory")
    public ResponseEntity<?> listInventory() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getPrincipal() instanceof UserDetailsImpl userDetail) {

            try {
                String username = userDetail.getUsername();
                String email = userDetail.getEmail();

                List<Product> allProducts = (List<Product>) productRepository.findAll();

                Long totalProductsCount = productRepository.count();

                List<Product> outOfStockProducts = allProducts.stream()
                        .filter(p -> p.getInv() == 0)
                        .toList();

                List<Product> lowInStockProducts = allProducts.stream()
                        .filter(p -> p.getInv() > 0 && p.getInv() < p.getMinInv())
                        .toList();

                List<Object[]> manufacturerCount = productRepository.countProductsByManufacturer();

                Map<String, Long> manufacturerMap = new HashMap<>();
                for (Object[] countData : manufacturerCount) {
                    String manufacturer = (String) countData[0];
                    Long total = (Long) countData[1];
                    manufacturerMap.put(manufacturer, total);
                }

                InventoryDTO inventoryDTO = new InventoryDTO(username, email, allProducts, totalProductsCount, outOfStockProducts, lowInStockProducts, manufacturerMap);

                return ResponseEntity.ok(inventoryDTO);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred while fetching inventory data.");
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized: Access is denied");
        }
    }
}
