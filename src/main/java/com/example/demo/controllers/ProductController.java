package com.example.demo.controllers;

import com.example.demo.domain.Product;
import com.example.demo.dto.ApiResponseDTO;
import com.example.demo.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    private ApplicationContext context;
    private final ProductService productService;
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/addproduct")
    public ResponseEntity<?> addProduct(@Valid @RequestBody Product product, Errors errors) {
        if (errors.hasErrors()) {
            // Collect error messages and return them
            String errorMsg = errors.getAllErrors().stream()
                    .map(ObjectError::getDefaultMessage)
                    .collect(Collectors.joining(", "));
            return ResponseEntity.badRequest().body(errorMsg);
        } else {
            // Process the price and save the product
            String price = String.valueOf(product.getPrice()).replace(",", "");
            product.setPrice(Double.parseDouble(price));
            productService.save(product);

            // Return a success response in JSON format
            Map<String, Object> response = new HashMap<>();
            response.put("message", "Product added successfully");
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        }
    }

    @PutMapping("/updateproduct")
    public ResponseEntity<?> updateProduct(@Valid @RequestBody Product product, Errors errors) {
        logger.debug("Calling updateProduct with product: " + product);

        if (errors.hasErrors()) {
            // Collect error messages and return them
            String errorMsg = errors.getAllErrors().stream()
                    .map(ObjectError::getDefaultMessage)
                    .collect(Collectors.joining(", "));
            return ResponseEntity.badRequest().body(errorMsg);
        } else {
            try {
                long theId = product.getId();
                logger.debug("Fetched productID: " + theId);
                Product theProduct = productService.findById(Math.toIntExact(theId));
                logger.debug("Fetched theProduct: " + theProduct);
                // Process the price and save the product
                String price = String.valueOf(product.getPrice()).replace(",", "");
                product.setPrice(Double.parseDouble(price));
                productService.save(product);

                ApiResponseDTO<Product> apiResponseDTO = new ApiResponseDTO<>(true, "Product updated successfully.", product);

                return ResponseEntity.ok(apiResponseDTO);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error updating product with productId: " + product.getId());
            }
        }
    }


    @GetMapping("/getproduct")
    public ResponseEntity<?> getProduct(@RequestParam("productId") int theId) {
        try {
            Product theProduct = productService.findById(theId);
            return ResponseEntity.ok(theProduct);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error fetching product.");
        }
    }

    @DeleteMapping("/deleteproduct")
    public  ResponseEntity<?> deleteProduct(@RequestParam("productID") int theId) {
        try {
            productService.deleteById(theId);
            return ResponseEntity.ok("Deleted product with productID: " + theId + " successfully");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error deleting product with productID: " + theId);
        }
    }

    @GetMapping("/buyproduct")
    public String buyProduct(@RequestParam("productId") int productId, RedirectAttributes redirectAttributes) {
        Product product = productService.findById(productId);
        int inv = product.getInv();
        if (inv > 0) {
            product.setInv(inv - 1);
            productService.save(product);
            redirectAttributes.addFlashAttribute("message", "Purchase successful!");
            redirectAttributes.addFlashAttribute("alertClass", "alert-success");
        } else {
            redirectAttributes.addFlashAttribute("message", "Purchase failed: Product out of stock.");
            redirectAttributes.addFlashAttribute("alertClass", "alert-danger");
        }
        return "redirect:/mainscreen";
    }
}
