package com.example.demo.controllers;

import com.example.demo.dto.ReportsDTO;
import com.example.demo.service.CsvReportService;
import com.example.demo.service.PdfReportService;
import com.example.demo.service.UserDetailsImpl;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@CrossOrigin( origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class ReportsController {

    @Autowired
    private PdfReportService pdfReportService;

    @Autowired
    private CsvReportService csvReportService;

    @GetMapping("/reports")
    public ResponseEntity<?> listReports() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getPrincipal() instanceof UserDetailsImpl userDetail) {

        try {
            String username = userDetail.getUsername();
            String email = userDetail.getEmail();

            ReportsDTO reportsDTO = new ReportsDTO(username, email);

            return ResponseEntity.ok(reportsDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred while fetching reports data.");
        }} else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized: Access is denied.");
        }

    }

    @GetMapping("/reports/partsLowInStock")
    public ResponseEntity<?> downloadPdf() throws IOException, DocumentException {
        try {
        ByteArrayOutputStream baos = pdfReportService.generateLowStockReport();
        ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=LowStockReport.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred while fetching PDF");
        }
    }

    @GetMapping("/reports/partsLowInStockCsv")
    public ResponseEntity<?> downloadCsv() throws IOException {
        try {
        ByteArrayOutputStream baos = csvReportService.generateLowStockReport();
        ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=LowStockReport.csv");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.parseMediaType("text/csv"))
                .body(new InputStreamResource(bis));
    } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred while fetching CSV");
        }
    }
}
