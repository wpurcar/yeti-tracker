package com.example.demo.controllers;

import com.example.demo.domain.Product;
import com.example.demo.dto.SalesDTO;
import com.example.demo.service.SaleServiceImpl;
import com.example.demo.service.UserDetailsImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin( origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class SalesController {
    private final SaleServiceImpl saleServiceImpl;

    public SalesController(SaleServiceImpl saleServiceImpl) {
        this.saleServiceImpl = saleServiceImpl;
    }
    @GetMapping("/sales")
    public ResponseEntity<?> listSales() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.getPrincipal() instanceof UserDetailsImpl userDetail) {
            try {
            String username = userDetail.getUsername();
            String email = userDetail.getEmail();

            double monthlySalesRevenue = saleServiceImpl.calculateMonthlySalesRevenue();
            double yearToDateSales = saleServiceImpl.calculateYearToDateSales();
            List<Product> topSellingProducts = saleServiceImpl.findTopSellingProducts();
            double inventoryTurnoverRate = saleServiceImpl.calculateInventoryTurnoverRate();

            SalesDTO salesDTO = new SalesDTO(username, email, monthlySalesRevenue, yearToDateSales, topSellingProducts, inventoryTurnoverRate);

            return ResponseEntity.ok(salesDTO);
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred while fetching sales data.");
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized: Access is denied.");
        }
    }
}
