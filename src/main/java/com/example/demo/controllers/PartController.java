package com.example.demo.controllers;

import com.example.demo.domain.InhousePart;
import com.example.demo.domain.OutsourcedPart;
import com.example.demo.domain.Part;
import com.example.demo.dto.ApiResponseDTO;
import com.example.demo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin( origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class PartController {

    @Autowired
    private ApplicationContext context;

    @DeleteMapping("/deletepart")
    public ResponseEntity<?> deletePart(@RequestParam("partId") int theId) {
        try {
            PartService repo = context.getBean(PartServiceImpl.class);
            Part part = repo.findById(theId);

            if (part.getProducts().isEmpty()) {
                repo.deleteById(theId);
                ApiResponseDTO<Part> apiResponseDTO = new ApiResponseDTO<>(true, "Deleted part successfully", part);
                return ResponseEntity.ok(apiResponseDTO);
            } else {
                // Return a response indicating the part is associated with products
                return ResponseEntity.status(HttpStatus.CONFLICT).body("Part is associated with products and cannot be deleted");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error deleting part with partID: " + theId);
        }
    }
}
