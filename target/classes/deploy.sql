-- Drop the existing "hibernate_sequence" sequence if it exists
DROP SEQUENCE IF EXISTS hibernate_sequence;

-- Create the "hibernate_sequence" sequence
CREATE SEQUENCE hibernate_sequence START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;

CREATE TABLE parts
(
    part_type    INTEGER NOT NULL,
    id           BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('hibernate_sequence'),
    inv          INTEGER NOT NULL,
    max_inv      INTEGER NOT NULL,
    min_inv      INTEGER NOT NULL,
    name         VARCHAR(255),
    price        DOUBLE PRECISION NOT NULL,
    part_id      INTEGER,
    company_name VARCHAR(255),
    CHECK (inv >= 0),
    CHECK (max_inv <= 200),
    CHECK (min_inv >= 10),
    CHECK (price >= 0.0)
);

CREATE TABLE products
(
    id    BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('hibernate_sequence'),
    inv   INTEGER NOT NULL,
    name  VARCHAR(255),
    price DOUBLE PRECISION NOT NULL,
    CHECK (inv >= 0),
    CHECK (price >= 0.0)
);

CREATE TABLE product_part
(
    part_id    BIGINT NOT NULL,
    product_id BIGINT NOT NULL,
    PRIMARY KEY (part_id, product_id),
    FOREIGN KEY (product_id) REFERENCES products,
    FOREIGN KEY (part_id) REFERENCES parts
);

CREATE TABLE roles
(
    id   BIGSERIAL PRIMARY KEY,
    name VARCHAR(20)
);

CREATE TABLE users
(
    id       BIGSERIAL PRIMARY KEY,
    email    VARCHAR(50) UNIQUE,
    password VARCHAR(120),
    username VARCHAR(20) UNIQUE
);

CREATE TABLE refreshtoken
(
    id          BIGINT NOT NULL PRIMARY KEY,
    expiry_date TIMESTAMP NOT NULL,
    token       VARCHAR(255) NOT NULL UNIQUE,
    user_id     BIGINT,
    FOREIGN KEY (user_id) REFERENCES users
);

CREATE TABLE user_roles
(
    user_id BIGINT NOT NULL,
    role_id BIGINT NOT NULL,
    PRIMARY KEY (user_id, role_id),
    FOREIGN KEY (role_id) REFERENCES roles,
    FOREIGN KEY (user_id) REFERENCES users
);

INSERT INTO roles (name) VALUES ('ROLE_USER'), ('ROLE_MODERATOR'), ('ROLE_ADMIN');
-- InhouseParts
INSERT INTO parts (part_type, inv, max_inv, min_inv, name, price, part_id, company_name)
VALUES
    (1, 94, 200, 10, '20x20 Air Filter, MERV 8 pleated (5 pack)', 12.96, NULL, NULL),
    (1, 93, 200, 20, '40/5 μf 370 Volt VAC - Dual Run Capacitor', 43.25, NULL, NULL),
    (1, 95, 200, 20, '5A Fuse, (5 pack)', 18.86, NULL, NULL);

-- OutsourcedParts
INSERT INTO parts (part_type, inv, max_inv, min_inv, name, price, part_id, company_name)
VALUES
    (2, 12, 50, 11, 'R-410A Refrigerant 25LB', 375.00, NULL, NULL),
    (2, 84, 200, 30, 'Honeywell 2H/1C, Standard Display Thermostat', 53.18, NULL, NULL);

-- Products
INSERT INTO products (inv, name, price)
VALUES
    (12, '18,000 BTU 1.5 Ton 14 SEER Goodman Air Conditioner Standard Split System', 2103.00),
    (11, '24,000 BTU 2 Ton 14 SEER Goodman Air Conditioner Split System', 2568.00),
    (8, '30,000 BTU 2.5 Ton 14 SEER Goodman Heat Pump Air Conditioner System', 2913.00),
    (12, '36,000 BTU 3 Ton 14 SEER Goodman Heat Pump', 2196.00),
    (9, '42,000 BTU 3.5 Ton 14 SEER Goodman Air Conditioner Split System', 2973.00);
