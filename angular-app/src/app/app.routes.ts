import {Routes} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {MainscreenComponent} from "./mainscreen/mainscreen.component";
import {SalesComponent} from "./sales/sales.component";
import {InventoryComponent} from "./inventory/inventory.component";
import {ReportsComponent} from "./reports/reports.component";
import {ProductFormComponent} from "./product-form/product-form.component";
import {RegisterFormComponent} from "./register/register.component";
import {InhousePartFormComponent} from "./inhouse-part-form/inhouse-part-form.component";
import {OutsourcedPartFormComponent} from "./outsourced-part-form/outsourced-part-form.component";



export const routes: Routes = [
    { path: "login", title: 'Login', component: LoginComponent},
    { path: "register", title: 'Register', component: RegisterFormComponent},
    { path: "mainscreen", title: 'Home', component: MainscreenComponent},
    { path: "inventory", title: 'Inventory', component: InventoryComponent},
    { path: "sales", title: 'Sales', component: SalesComponent},
    { path: "reports", title: 'Reports', component: ReportsComponent},
    { path: 'product-form', title: 'Product Detail', component: ProductFormComponent },
    { path: 'product-form/:id', title: 'Product Detail', component: ProductFormComponent },
    { path: 'inhouse-part-form', title: 'Inhouse Part Detail', component: InhousePartFormComponent },
    { path: 'inhouse-part-form/:id', title: 'Inhouse Part Detail', component: InhousePartFormComponent },
    { path: 'outsourced-part-form', title: 'Outsourced Part Detail', component: OutsourcedPartFormComponent },
    { path: 'outsourced-part-form/:id', title: 'Outsourced Part Detail', component: OutsourcedPartFormComponent },
    { path: '**', redirectTo: '/mainscreen' }
];
