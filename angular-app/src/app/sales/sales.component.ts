import {Component, OnInit} from '@angular/core';
import {CommonModule} from "@angular/common";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";

interface ISalesData {
  username: string;
  email: string;
  monthlySalesRevenue: number;
  yearToDateSales: number;
  topSellingProducts: any[];
  inventoryTurnoverRate: number;
}
@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  standalone: true,
  imports: [ HttpClientModule, CommonModule ],
  styleUrl: './sales.component.scss'
})
export class SalesComponent implements OnInit {
  username: string = '';
  email: string = '';
  monthlySalesRevenue: number = 0;
  yearToDateSales: number = 0;
  topSellingProducts: any[] = [];
  inventoryTurnoverRate: number = 0;


  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService) {}


  ngOnInit(): void {
    this.fetchSalesData();
  }

  fetchSalesData() {
    this.http.get<ISalesData>(`http://localhost:8080/api/sales`, {
      withCredentials: true
    }).subscribe({
      next: (data) => {
        this.username = data.username;
        this.email = data.email;
        this.monthlySalesRevenue = data.monthlySalesRevenue;
        this.yearToDateSales = data.yearToDateSales;
        this.topSellingProducts = data.topSellingProducts;
        this.inventoryTurnoverRate = data.inventoryTurnoverRate;
      },
      error: (error) => {
        this.showError(`Error fetching sales data`, error);
      }
    })
  }

  signOut() {
    this.http.post('http://localhost:8080/api/auth/signout', {}, {
      withCredentials: true,
      responseType: 'json' })
      .subscribe({
        next: () => {
          this.showSuccess(`Successfully signed out`);
          this.router.navigate(['/login'])
        },
        error: error =>{
          this.showError(`Error signing out`, error);
        }
      });
  }

  showError(message: string, error: any) {
    console.error(`${message}, ${error}`);
    this.toastr.error(message);
  }

  showSuccess(message: string) {
    console.log(message);
    // alert(message);
    this.toastr.success(message);
  }

}
