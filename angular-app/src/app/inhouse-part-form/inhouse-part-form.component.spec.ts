import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InhousePartFormComponent } from './inhouse-part-form.component';

describe('InhousePartFormComponent', () => {
  let component: InhousePartFormComponent;
  let fixture: ComponentFixture<InhousePartFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [InhousePartFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(InhousePartFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
