import {Component, OnInit} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule, HttpParams, HttpResponse} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";

interface IInhousePart {
  id?: number;
  sku: string;
  name: string;
  price: number;
  inv: number;
  minInv: number;
  maxInv: number;
}
@Component({
  selector: 'app-inhouse-part-form',
  standalone: true,
    imports: [
        FormsModule, HttpClientModule
    ],
  templateUrl: './inhouse-part-form.component.html',
  styleUrl: './inhouse-part-form.component.scss'
})
export class InhousePartFormComponent implements OnInit {
  part = {
    sku: '',
    name: '',
    price: '',
    inv: '',
    minInv: '',
    maxInv: '',
  };

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const partId = parseInt(<string>params.get('id'));
      if (partId) {
        try {
          this.getPart(partId);
        } catch (e) {
          this.showError(`Error fetching part`, e);
        }
      }
    })
  }


  getPart(partId: number) {
    const params = new HttpParams().set('partId', partId.toString());

    this.http.get<IInhousePart>(`http://localhost:8080/api/inhouse/getpart`, {
      params,
      observe: 'response',
      withCredentials: true
    })
      .subscribe({
        next: (response: HttpResponse<IInhousePart>) => {
          console.log(`Response: `, response);
          if (response.status === 200 && response.body) {
            this.part = {
              ...response.body,
              price: response.body.price.toString(),
              inv: response.body.inv.toString(),
              minInv: response.body.minInv.toString(),
              maxInv: response.body.maxInv.toString()
            };
            console.log(`Part fetched successfully: `, this.part);
            this.showSuccess(`Part fetched successfully`);
          }
        },
        error: (error: any) => {
          this.showError(`Error fetching part with partId ${partId}`, error);
        }
      })
  }

  validateAndSubmit() {
    // Regular expression to remove unwanted characters
    const sanitizeRegex = /[,'"`]/g;

    // Sanitize the input fields
    const sanitizedSku = this.part.sku.replace(sanitizeRegex, '');
    const sanitizedName = this.part.name.replace(sanitizeRegex, '');
    const price = parseFloat(this.part.price.replace(/,/g, ''));
    const inv = parseInt(this.part.inv);
    const minInv = parseInt(this.part.minInv);
    const maxInv = parseInt(this.part.maxInv);

    if (isNaN(price) || price <= 0) {
      this.showError('Price must be a positive number', null);
      return;
    }

    if (isNaN(inv) || inv < 0) {
      this.showError('Inventory must be a non-negative number', null);
      return;
    }

    if (isNaN(minInv) || minInv < 0) {
      this.showError('Minimum inventory must be a non-negative number', null);
      return;
    }

    if (isNaN(maxInv) || maxInv < 0) {
      this.showError('Maximum inventory must be a non-negative number', null);
      return;
    }

    let newPart: IInhousePart = {
      sku: sanitizedSku,
      name: sanitizedName,
      price: price,
      inv: inv,
      minInv: minInv,
      maxInv: maxInv
    };


    console.log('Form is valid and ready for submission: ', newPart);

    this.route.paramMap.subscribe(params => {
      const partId = parseInt(<string>params.get('id'));
      if (partId) {
        newPart.id = partId;
        this.updatePart(newPart);
      } else {
        this.addPart(newPart);
      }
    })
  }

  addPart(part: IInhousePart) {
    this.http.post('http://localhost:8080/api/inhouse/addpart', part, {
      observe: 'response', // Get the full HttpResponse
      withCredentials: true
    })
      .subscribe({
        next: (response: HttpResponse<any>) => {
          console.log(`Response: `, response);

          // Check the status code here
          if (response.status === 201) {
            console.log(`Part added successfully: `, response.body);
            this.showSuccess('Part added successfully');
            this.router.navigate([`/mainscreen`]);
          }
        },
        error: (error: any) => {
          this.showError('Failed to add the part', error);
        }
      })
  }

  updatePart(part: IInhousePart) {

    this.http.put('http://localhost:8080/api/inhouse/updatepart', part, {
      observe: 'response', // Get the full HttpResponse
      withCredentials: true
    })
      .subscribe({
        next: (response: HttpResponse<any>) => {
          console.log(`Response: `, response);

          // Check the status code here
          if (response.status === 200) {
            this.showSuccess('Part updated successfully');
            this.router.navigate([`/mainscreen`]);
          }
        },
        error: (error: any) => {
          this.showError('Failed to update the part', error);
        }
      })
  }


  showError(message: string, error: any) {
    console.error(`${message}, ${error}`);
    this.toastr.error(message);
  }

  showSuccess(message: string) {
    console.log(message);
    // alert(message);
    this.toastr.success(message);
  }


}
