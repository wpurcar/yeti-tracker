import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpClientModule, HttpParams, HttpResponse} from '@angular/common/http';
import {Router, RouterLink} from '@angular/router';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ToastrService} from "ngx-toastr";

interface IMainScreenData {
  partDTOList: any[];
  username: string;
  email: string;
  parts: any[]; // Replace with appropriate type if available
  products: any[]; // Replace with appropriate type if available
}

@Component({
  selector: 'app-mainscreen',
  templateUrl: './mainscreen.component.html',
  standalone: true,
  imports: [HttpClientModule, CommonModule, RouterLink, FormsModule],
  styleUrls: ['./mainscreen.component.scss']
})

export class MainscreenComponent implements OnInit {
  username: string = '';
  email: string = '';
  partkeyword: string = '';
  productkeyword: string = '';
  // parts: any[] = [];
  products: any[] = [];
  partDTOList: any[] = [];

  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService) {}

  ngOnInit(): void {
    this.fetchMainScreenData();
  }

  fetchMainScreenData() {
    this.http.get<IMainScreenData>('http://localhost:8080/api/mainscreen', {
      params: { partkeyword: this.partkeyword, productkeyword: this.productkeyword },
      withCredentials: true
    }).subscribe({
      next: (data: IMainScreenData) => {
        this.username = data.username;
        this.email = data.email;
        this.partDTOList = data.partDTOList;
        this.products = data.products;
      },

      error: (error) => {
        this.showError(`Error fetching mainscreen data`, error);

        if (error.status === 401) {
          this.showError(`Please login and try again`, error);
          this.router.navigate(['/login']);
        }
      }
    });
  }

  deleteProduct(productId: string) {
    const params = new HttpParams().set('productID', productId);

    this.http.delete('http://localhost:8080/api/deleteproduct', {
      params: params,
      observe: 'response',
      withCredentials: true
    }).subscribe({
      next: (response: HttpResponse<any>) => {
        if (response.status === 200) {
          this.showSuccess(`Deleted product with productID: ${productId} successfully`);
        }
      },
      error: (error) => {
        this.showError(`Error deleting product with productId: ${productId}:`, error);
        if (error.status === 401) {
          this.showError(`Please login and try again`, error);
          this.router.navigate(['/login']);
        }
      }
    });
  }

  deletePart(partId: string) {
    const params = new HttpParams().set('partId', partId);

    this.http.delete('http://localhost:8080/api/deletepart', {
      params: params,
      observe: 'response',
      withCredentials: true
    }).subscribe({
      next: (response: HttpResponse<any>) => {
        if (response.status === 200) {
          this.showSuccess(`Deleted part with partID: ${partId} successfully`);
        }
      },
      error: (error) => {
        this.showError(`Error deleting part with partId: ${partId}`, error);
        if (error.status === 401) {
          this.router.navigate(['/login']);
        }
      }
    });
  }

  signOut() {
    this.http.post('http://localhost:8080/api/auth/signout', {}, {
      withCredentials: true,
      responseType: 'json' })
      .subscribe({
        next: () => {
          this.showSuccess(`Successfully signed out`);
          this.router.navigate(['/login'])
        },
        error: error =>{
          this.showError(`Error signing out`, error);
        }
      });
  }

  clearPartSearch() {
    this.partkeyword = '';
    try {
      this.fetchMainScreenData();
    } catch (e) {
      this.showError(`Error clearing search`, e);
    }

  }

  clearProductSearch() {
    this.productkeyword = '';
    try {
      this.fetchMainScreenData();
    } catch (e) {
      this.showError(`Error clearing search`, e);
    }

  }


  showError(message: string, error: any) {
    console.error(`${message}, ${error}`);
    this.toastr.error(message);
  }

  showSuccess(message: string) {
    console.log(message);
    // alert(message);
    this.toastr.success(message);
  }

}
