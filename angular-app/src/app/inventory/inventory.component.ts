import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {Router} from "@angular/router";
import {CommonModule, NgForOf} from "@angular/common";
import {ToastrService} from "ngx-toastr";

interface IInventoryData {
  username: string;
  email: string;
  allProducts: any[];
  totalProductsCount: number;
  outOfStockProducts: any[];
  lowInStockProducts: any[];
  manufacturerMap: any[];
}

@Component({
  selector: 'app-inventory',
  standalone: true,
  imports: [
    NgForOf, CommonModule, HttpClientModule
  ],
  templateUrl: './inventory.component.html',
  styleUrl: './inventory.component.scss'
})
export class InventoryComponent implements OnInit {
  username: string = '';
  email: string = '';
  allProducts: any[] = [];
  totalProductsCount: number = 0;
  outOfStockProducts: any[] = [];
  lowInStockProducts: any[] = [];
  manufacturerMap: any[] = [];

  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService) {}

  ngOnInit(): void {
    this.fetchInventoryData();
  }

  fetchInventoryData() {
    this.http.get<IInventoryData>(`http://localhost:8080/api/inventory`, { withCredentials: true }).subscribe({
      next: (data) => {
        console.log(`Fetched inventory data: `, data);
        this.username = data.username;
        this.email = data.email;
        this.allProducts = data.allProducts;
        this.totalProductsCount = data.totalProductsCount;
        this.outOfStockProducts = data.outOfStockProducts;
        this.lowInStockProducts = data.lowInStockProducts;
        this.manufacturerMap = data.manufacturerMap;
      },
      error: (error) => {
        this.showError(`Error fetching inventory data`, error);
        if (error.status === 401) {
          this.showError(`Please login and try again`, error);
          this.router.navigate(['/login']);
        }
      }
    })
  }

  signOut() {
    this.http.post('http://localhost:8080/api/auth/signout', {}, {
      withCredentials: true,
      responseType: 'json' })
      .subscribe({
        next: () => {
          this.showSuccess(`Successfully signed out`);
          this.router.navigate(['/login'])
        },
        error: error =>{
          this.showError(`Error signing out`, error);
        }
      });
  }

  showError(message: string, error: any) {
    console.error(`${message}, ${error}`);
    this.toastr.error(message);
  }

  showSuccess(message: string) {
    console.log(message);
    // alert(message);
    this.toastr.success(message);
  }
}
