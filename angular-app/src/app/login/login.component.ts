import {Component} from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule, NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  standalone: true,
  imports: [
    FormsModule, HttpClientModule
  ],
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  username: string = '';
  password: string = '';
  usernameError: string = '';
  passwordError: string = '';
  loading: boolean = false;

  constructor(
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) {}

  onSubmit(form: NgForm) {
    if (form.invalid) {
      return;
    }

    this.login();
  }

  login() {
    this.resetErrors();
    const formData = { username: this.username, password: this.password };
    this.loading = true;

    this.http.post('http://localhost:8080/api/auth/signin', formData, { withCredentials: true })
      .subscribe({
        next: (data: any) => {
          this.loading = false;
          if (data.username) {
            this.showSuccess(`User logged in successfully!`);
            this.router.navigate(['/mainscreen']);
          } else {
            this.usernameError = data.message;
          }
        },
        error: (error: any) => {
          this.loading = false;
          this.showError('Error signing in', error);
        }
      });
  }

  resetErrors() {
    this.usernameError = '';
    this.passwordError = '';
  }

  showError(message: string, error: any) {
    console.error(`${message}, ${error}`);
    this.toastr.error(message);
  }

  showSuccess(message: string) {
    console.log(message);
    // alert(message);
    this.toastr.success(message);
  }

}
