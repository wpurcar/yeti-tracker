import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";

interface IReportsData {
  username: string;
  email: string;
}
@Component({
  selector: 'app-reports',
  standalone: true,
  imports: [HttpClientModule],
  templateUrl: './reports.component.html',
  styleUrl: './reports.component.scss'
})
export class ReportsComponent implements OnInit {
  username: string = '';
  email: string = '';

  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService) {}

  ngOnInit(): void {
        this.fetchReportsData();
    }

  fetchReportsData() {
    this.http.get<IReportsData>(`http://localhost:8080/api/reports`, {
      withCredentials: true
    }).subscribe({
      next: (data) => {
        this.username = data.username;
        this.email = data.email;
      },
      error: (error) => {
        this.showError(`Error fetching reports data`, error);
        if (error.status === 401) {
          this.showError(`Please login and try again`, error);
          this.router.navigate(['/login']);
        }
      }
    })
  }

  fetchPartsLowInStockPDF() {
    this.http.get('http://localhost:8080/api/reports/partsLowInStock', {
      responseType: 'blob', // Important for handling binary data
      withCredentials: true
    }).subscribe(blob => {
      const objectUrl = URL.createObjectURL(blob);
      window.open(objectUrl, '_blank'); // Open in a new tab
      URL.revokeObjectURL(objectUrl);
    }, error => {
      this.showError(`Error fetching PDF report`, error);
    });
  }


  fetchPartsLowInStockCSV() {
    this.http.get('http://localhost:8080/api/reports/partsLowInStockCsv', {
      responseType: 'blob', // Important for handling binary data
      withCredentials: true
    }).subscribe(blob => {
      const a = document.createElement('a');
      const objectUrl = URL.createObjectURL(blob);
      a.href = objectUrl;
      a.download = 'LowStockReport.csv';
      a.click();
      URL.revokeObjectURL(objectUrl);
    }, error => {
      this.showError(`Error fetching CSV report`, error);
    });
  }


  signOut() {
    this.http.post('http://localhost:8080/api/auth/signout', {}, {
      withCredentials: true,
      responseType: 'json' })
      .subscribe({
        next: () => {
          this.showSuccess(`Successfully signed out`);
          this.router.navigate(['/login'])
        },
        error: error =>{
          this.showError(`Error signing out`, error);
        }
      });
  }

  showError(message: string, error: any) {
    console.error(`${message}, ${error}`);
    this.toastr.error(message);
  }

  showSuccess(message: string) {
    console.log(message);
    // alert(message);
    this.toastr.success(message);
  }
}
