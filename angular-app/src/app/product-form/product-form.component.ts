import {Component, OnInit} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule, HttpParams, HttpResponse} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";

interface IProduct {
  id?: number;
  sku: string;
  name: string;
  manufacturer: string;
  refrigerant: string;
  price: number;
  inv: number;
  minInv: number;
  maxInv: number;
}
@Component({
  selector: 'app-product-form',
  standalone: true,
  imports: [FormsModule, HttpClientModule],
  templateUrl: './product-form.component.html',
  styleUrl: './product-form.component.scss'
})
export class ProductFormComponent implements OnInit {
  product = {
    sku: '',
    name: '',
    manufacturer: '',
    refrigerant: '',
    price: '',
    inv: '',
    minInv: '',
    maxInv: '',
  };

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const productId = parseInt(<string>params.get('id'));
      if (productId) {
        try {
          this.getProduct(productId);
        } catch (e) {
          this.showError(`No product found`, productId);
        }
      }
    })
  }

  getProduct(productId: number) {
    const params = new HttpParams().set('productId', productId.toString());

    this.http.get<IProduct>(`http://localhost:8080/api/getproduct`, {
      params,
      observe: 'response',
      withCredentials: true
    })
      .subscribe({
        next: (response: HttpResponse<IProduct>) => {
          console.log(`Response: `, response);
          if (response.status === 200 && response.body) {
            this.product = {
              ...response.body,
              price: response.body.price.toString(),
              inv: response.body.inv.toString(),
              minInv: response.body.minInv.toString(),
              maxInv: response.body.maxInv.toString()
            };
            console.log(`Product fetched successfully: `, this.product);
          }
        },
        error: (error: any) => {
          this.showError(`Failed to fetch the product with productId: ${productId}`, error);
        }
      })
  }


  validateAndSubmit() {
    // Regular expression to remove unwanted characters
    const sanitizeRegex = /[,'"`]/g;

    // Sanitize the input fields
    const sanitizedSku = this.product.sku.replace(sanitizeRegex, '');
    const sanitizedName = this.product.name.replace(sanitizeRegex, '');
    const sanitizedManufacturer = this.product.manufacturer.replace(sanitizeRegex, '');
    const sanitizedRefrigerant = this.product.refrigerant.replace(sanitizeRegex, '');
    const price = parseFloat(this.product.price.replace(/,/g, ''));
    const inv = parseInt(this.product.inv);
    const minInv = parseInt(this.product.minInv);
    const maxInv = parseInt(this.product.maxInv);

    if (isNaN(price) || price <= 0) {
      this.showError('Price must be a positive number', null);
      return;
    }

    if (isNaN(inv) || inv < 0) {
      this.showError('Inventory must be a non-negative number', null);
      return;
    }

    if (isNaN(minInv) || minInv < 0) {
      this.showError('Minimum inventory must be a non-negative number', null);
      return;
    }

    if (isNaN(maxInv) || maxInv < 0) {
      this.showError('Maximum inventory must be a non-negative number', null);
      return;
    }

    let newProduct: IProduct = {
      sku: sanitizedSku,
      name: sanitizedName,
      manufacturer: sanitizedManufacturer,
      refrigerant: sanitizedRefrigerant,
      price: price,
      inv: inv,
      minInv: minInv,
      maxInv: maxInv
    };


    console.log('Form is valid and ready for submission: ', newProduct);

    this.route.paramMap.subscribe(params => {
      const productId = parseInt(<string>params.get('id'));
      if (productId) {
        newProduct.id = productId;
        this.updateProduct(newProduct);
      } else {
        this.addProduct(newProduct);
      }
  })
  }


  addProduct(product: IProduct) {
    this.http.post('http://localhost:8080/api/addproduct', product, {
      observe: 'response', // Get the full HttpResponse
      withCredentials: true
    })
      .subscribe({
        next: (response: HttpResponse<any>) => {
          console.log(`Response: `, response);

          // Check the status code here
          if (response.status === 201) {
            console.log(`Product added successfully: `, response.body);
            this.showSuccess('Product added successfully');
            this.router.navigate([`/mainscreen`]);
          }
        },
        error: (error: any) => {
          this.showError('Error adding product', error);
        }
      })
  }

  updateProduct(product: IProduct) {

    this.http.put('http://localhost:8080/api/updateproduct', product, {
      observe: 'response', // Get the full HttpResponse
      withCredentials: true
    })
      .subscribe({
        next: (response: HttpResponse<any>) => {
          console.log(`Response: `, response);

          // Check the status code here
          if (response.status === 200) {
            console.log(`Product updated successfully`);
            this.showSuccess('Product updated successfully');
            this.router.navigate([`/mainscreen`]);
          }
        },
        error: (error: any) => {
          this.showError('Error updating product', error);
        }
      })
  }

  showError(message: string, error: any) {
    console.error(`${message}, ${error}`);
    this.toastr.error(message);
  }

  showSuccess(message: string) {
    console.log(message);
    // alert(message);
    this.toastr.success(message);
  }

}
