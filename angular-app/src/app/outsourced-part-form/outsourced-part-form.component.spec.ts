import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutsourcedPartFormComponent } from './outsourced-part-form.component';

describe('OutsourcedPartFormComponent', () => {
  let component: OutsourcedPartFormComponent;
  let fixture: ComponentFixture<OutsourcedPartFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OutsourcedPartFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(OutsourcedPartFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
