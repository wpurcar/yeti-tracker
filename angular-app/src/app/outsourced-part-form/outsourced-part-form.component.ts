import {Component, OnInit} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {HttpClient, HttpClientModule, HttpParams, HttpResponse} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";

interface IOutsourcedPart {
  id?: number;
  sku: string;
  name: string;
  price: number;
  manufacturer: string;
  inv: number;
  minInv: number;
  maxInv: number;
}

@Component({
  selector: 'app-outsourced-part-form',
  standalone: true,
    imports: [
        FormsModule, HttpClientModule
    ],
  templateUrl: './outsourced-part-form.component.html',
  styleUrl: './outsourced-part-form.component.scss'
})
export class OutsourcedPartFormComponent implements OnInit {
  part = {
    sku: '',
    name: '',
    price: '',
    manufacturer: '',
    inv: '',
    minInv: '',
    maxInv: '',
  };

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService) {}


  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const partId = parseInt(<string>params.get('id'));
      if (partId) {
        try {
          this.getPart(partId);
        } catch (e) {
          this.showError(`Error fetching part with partId: ${partId}`, null);
        }
      }
    })
  }

  getPart(partId: number) {
    const params = new HttpParams().set('partId', partId.toString());

    this.http.get<IOutsourcedPart>(`http://localhost:8080/api/outsourced/getpart`, {
      params,
      observe: 'response',
      withCredentials: true
    })
      .subscribe({
        next: (response: HttpResponse<IOutsourcedPart>) => {
          console.log(`Response: `, response);
          if (response.status === 200 && response.body) {
            this.part = {
              ...response.body,
              price: response.body.price.toString(),
              manufacturer: response.body.manufacturer.toString(),
              inv: response.body.inv.toString(),
              minInv: response.body.minInv.toString(),
              maxInv: response.body.maxInv.toString()
            };
            console.log(`Outsourced part fetched successfully: `, this.part);
          }
        },
        error: (error: any) => {
          this.showError(`Failed to fetch the outsourced partId ${partId}`, error);
        }
      })
  }

  validateAndSubmit() {
    // Regular expression to remove unwanted characters
    const sanitizeRegex = /[,'"`]/g;

    // Sanitize the input fields
    const sanitizedSku = this.part.sku.replace(sanitizeRegex, '');
    const sanitizedName = this.part.name.replace(sanitizeRegex, '');
    const sanitizedManufacturer = this.part.manufacturer.replace(sanitizeRegex, '');
    const price = parseFloat(this.part.price.replace(/,/g, ''));
    const inv = parseInt(this.part.inv);
    const minInv = parseInt(this.part.minInv);
    const maxInv = parseInt(this.part.maxInv);

    if (isNaN(price) || price <= 0) {
      this.showError('Price must be a positive number', null);
      return;
    }

    if (isNaN(inv) || inv < 0) {
      this.showError('Inventory must be a non-negative number', null);
      return;
    }

    if (isNaN(minInv) || minInv < 0) {
      this.showError('Minimum inventory must be a non-negative number', null);
      return;
    }

    if (isNaN(maxInv) || maxInv < 0) {
      this.showError('Maximum inventory must be a non-negative number', null);
      return;
    }

    let newPart: IOutsourcedPart = {
      sku: sanitizedSku,
      name: sanitizedName,
      manufacturer: sanitizedManufacturer,
      price: price,
      inv: inv,
      minInv: minInv,
      maxInv: maxInv
    };


    console.log('Form is valid and ready for submission: ', newPart);

    this.route.paramMap.subscribe(params => {
      const partId = parseInt(<string>params.get('id'));
      if (partId) {
        newPart.id = partId;
        this.updatePart(newPart);
      } else {
        this.addPart(newPart);
      }
    })
  }

  addPart(part: IOutsourcedPart) {
    this.http.post('http://localhost:8080/api/outsourced/addpart', part, {
      observe: 'response', // Get the full HttpResponse
      withCredentials: true
    })
      .subscribe({
        next: (response: HttpResponse<any>) => {
          console.log(`Response: `, response);

          // Check the status code here
          if (response.status === 201) {
            console.log(`Part added successfully: `, response.body);
            this.showSuccess('Part added successfully');
            this.router.navigate([`/mainscreen`]);
          }
        },
        error: (error: any) => {
          this.showError('Error adding part', error);
        }
      })
  }

  updatePart(part: IOutsourcedPart) {

    this.http.put('http://localhost:8080/api/outsourced/updatepart', part, {
      observe: 'response', // Get the full HttpResponse
      withCredentials: true
    })
      .subscribe({
        next: (response: HttpResponse<any>) => {
          console.log(`Response: `, response);

          // Check the status code here
          if (response.status === 200) {
            console.log(`Part updated successfully`);
            this.showSuccess('Part updated successfully');
            this.router.navigate([`/mainscreen`]);
          }
        },
        error: (error: any) => {
          this.showError('Error updating part', error);
        }
      })
  }


  showError(message: string, error: any) {
    console.error(`${message}, ${error}`);
    this.toastr.error(message);
  }

  showSuccess(message: string) {
    console.log(message);
    // alert(message);
    this.toastr.success(message);
  }

}
