import { Component } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
  ɵFormGroupValue,
  ɵTypedOrUntyped
} from "@angular/forms";
import {HttpClient, HttpClientModule, HttpResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {CommonModule} from "@angular/common";
import {ToastrService} from "ngx-toastr";

interface IUser {
  username: string;
  email: string;
  password: string;
}
@Component({
  selector: 'app-register',
  standalone: true,
  imports: [HttpClientModule, ReactiveFormsModule, CommonModule],
  templateUrl: './register.component.html',
  styleUrl: './register.component.scss'
})
export class RegisterFormComponent {
  registerForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  });

  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService) {}

  validateAndSubmit() {
    if (this.registerForm.valid) {
      const newUser = this.registerForm.value;
      console.log('Form is valid and ready for submission: ', newUser);
      this.signup(newUser);
    }
  }

  signup(newUser: ɵTypedOrUntyped<{
    password: FormControl<string | null>;
    email: FormControl<string | null>;
    username: FormControl<string | null>
  }, ɵFormGroupValue<{
    password: FormControl<string | null>;
    email: FormControl<string | null>;
    username: FormControl<string | null>
  }>, any>) {
    this.http.post('http://localhost:8080/api/auth/signup', newUser, {
      observe: 'response',
      withCredentials: true
    })
      .subscribe({
        next: (response: HttpResponse<any>) => {
          console.log(`Response: `, response);

          // Check the status code here
          if (response.status === 200) {
            this.showSuccess('User registered successfully');
            this.router.navigate([`/login`]);
          }
        },
        error: (error: any) => {
          this.showError('Failed to register user', error);
        }
      })
  }

  showError(message: string, error: any) {
    console.error(`${message}, ${error}`);
    this.toastr.error(message);
  }

  showSuccess(message: string) {
    console.log(message);
    // alert(message);
    this.toastr.success(message);
  }
}
